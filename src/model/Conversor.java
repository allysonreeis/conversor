package model;

public class Conversor {
	public double celsiusParaFahreinheit(){
		double resultado = (9*45 + 160)/5;;
		return resultado;
	}
	public double celsiusParaFahreinheit(double valorCelsius){
		double resultado = (9*valorCelsius + 160)/5;
		return resultado;
	}
	public double celsiusParaKelvin(double valorCelsius){
		double resultado = valorCelsius + 273.15;
		return resultado;
	}
	public double fahreinheitParaCelsius(double valorFahreinheit){
		double resultado = (5*valorFahreinheit - 160)/9;
		return resultado;
	}
	public double fahreinheitParaKelvin (double valorFahreinheit) {
		double resultado = ((valorFahreinheit-32)/1.8) + 273.15;
		return resultado;
	}
	public double kelvinParaCelsius (double valorKelvin) {
		double resultado = valorKelvin - 273.15;
		return resultado; 
		
	}
	public double kelvinParaFahreinheit (double valorKelvin) {
		double resultado = ((valorKelvin - 273.15)*1.8) + 32;
		return resultado;
	}

}
