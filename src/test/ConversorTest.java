package test;

import static org.junit.Assert.*;
import model.Conversor;

import org.junit.Before;
import org.junit.Test;

public class ConversorTest {

	Conversor umConversor;
	@Before
	public void setUp() throws Exception {
		umConversor = new Conversor();
	}

	@Test
	public void testCelsiusParaFahreinheit () {
		assertEquals(122.0, umConversor.celsiusParaFahreinheit(50.0), 0.01);
	}
	
	@Test
	public void testCelsiusParaKelvin () {
		assertEquals(323.15, umConversor.celsiusParaKelvin(50), 0.01);
	}
	
	@Test
	public void testFahreinheitParaCelsius () {
		assertEquals(10.0, umConversor.fahreinheitParaCelsius(50), 0.01);
	}
	
	@Test
	public void testFahreinheitParaKelvin () {
		assertEquals(283.15, umConversor.fahreinheitParaKelvin(50), 0.01);
	} 
	
	@Test
	public void testKelvinParaCelsius () {
		assertEquals(-223.15, umConversor.kelvinParaCelsius(50), 0.01);
	}
	
	@Test
	public void testKelvinParaFahreinheit () {
		assertEquals(-369.67, umConversor.kelvinParaFahreinheit(50), 0.01);
	}

}
